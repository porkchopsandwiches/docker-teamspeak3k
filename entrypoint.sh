#!/usr/bin/env bash

set -e

function stop {
	kill $(ps -C ts3server -o pid= | awk '{ print $1; }')
	exit
}

trap stop INT
trap stop TERM

cd "${TS_DIRECTORY}/teamspeak3-server_linux_amd64/"
rm -rf ts3server.sqlitedb ts3server.sqlitedb-shm ts3server.sqlitedb-wal files logs ts3server.pid
if [ -e serverkey.dat ]; then
	mv serverkey.dat /data
fi

mkdir -p /data/files /data/logs

for i in $(ls /data)
do
	ln -sf /data/${i}
done

find -L "${TS_DIRECTORY}/teamspeak3-server_linux_amd64/" -type l -delete

export LD_LIBRARY_PATH=".:$LD_LIBRARY_PATH"
exec /tini -- ./ts3server $@
